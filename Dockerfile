FROM mysql:5.6

MAINTAINER Miguel Lagos <angellagoscubas2209@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
